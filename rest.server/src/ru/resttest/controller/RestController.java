package ru.resttest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RestController {

    private static final String AMOUNT_URL = "/amount";
    
	public RestController() {
	}

    @RequestMapping(value = AMOUNT_URL, method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
	public Long getAmount() {
		return new Long(11);
	}
	
}
